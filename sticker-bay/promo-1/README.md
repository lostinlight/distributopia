
#### Promo materials 1

##### Author
@lostinlight: https://mastodon.xyz/@lightone

##### *Fediverse.party* image
* Uses some vector graphics from [Pixabay](https://pixabay.com) [CC0 license]
* LICENSE: Safely use this image - it's CC0.

##### *Friendica promo bubbles* image
* Uses Friendica official Flaxy O'Hare mascot, authored by @lostinlight, which is under CC0 license
* LICENSE: Safely use this image - it's CC0.

##### *Friendica keep in contact* image
* LICENSE: Safely use this image - it's CC0.

##### *Xmas in Fediverse* image
* Uses some vector graphics from [Pixabay](https://pixabay.com) and [Public Domain Vectors](https://publicdomainvectors.org) [CC0 license]
* Elephant SVG kindly provided by [@rash](https://chaos.social/@rash)

##### *Friendica happy BDay* image
* LICENSE: Safely use this image - it's CC0.

##### *You are in Fediverse* image
* LICENSE: Safely use this image - it's CC0.

##### *Fe-nix* image and its variations
* LICENSE: Safely use this image, crediting the author - CC-BY.

##### *Fedi-YaY* image
* LICENSE: Use this image, crediting the author; if you modify it, release it under the same license - CC-BY-SA.

##### *PeerTube Hygiene* image
* LICENSE: Use this image, crediting the author; if you modify it, release it under the same license - CC-BY-SA (based on license of the original mascot author David Revoy).

##### *Common Mission* image
* LICENSE: Use this image, crediting the author - CC-BY.

##### *Purple party 13/2021* image
* LICENSE: Use this image, crediting the author; if you modify it, release it under the same license - CC-BY-SA.

##### *Mascots 13/2021* image
* LICENSE: Use this image, crediting the author; if you modify it, release it under the same license - CC-BY-SA (based on license of David Revoy who created PeerTube and Mobilizon mascots)

##### Credits
* Mastodon elephants from the official [press kit](https://joinmastodon.org/press-kit.zip) and from *joinmastodon.org* website, by its respective author
* Levitating GNU by [Nevrax Design Team](https://www.gnu.org/graphics/meditate.html)
* PeerTube octopus and Mobilizon fennec based on [David Revoy](https://www.davidrevoy.com)'s artwork.
* Misskey Ai mascot - author and license unknown

#### Note on license
Some images contain graphics with unknown license. Most likely you'll have no problems using this work or any work based on it in your promotional materials and blog posts.
