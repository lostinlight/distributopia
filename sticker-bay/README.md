
# :pizza: Sticker Bay

# Stickers - grab them and print them

#### Sticker Pack 1 folder
![preview screen 1](stickerpack-1.png?raw=true)

#### Sticker Pack 2 Iday folder
![preview screen 2](stickerpack-2-iday.jpg?raw=true)

#### Promo 1 folder
![preview screen 3](promo-1.jpg?raw=true)

#### Sticker Pack 3 Pleroma folder
![preview screen 3](stickerpack-3.png?raw=true)

#### License
See license file or README file in each folder

#### Contributors
* [@lostinlight](https://mastodon.xyz/@lightone)
* [@iday](https://mastodonten.de/@iday)
* [Victor von Void](@https://mastodon.technology/@vvv)

## Contributing

**Add your sticker packs, postcards, posters and other promotional Fediverse materials!**

Add a new folder (for example, `stickerpack-3-your-name` or `promo-2-your-name`) with your materials, license file, and READMe file with authors, credits, comments. Thank you!

If you modify existing materials and believe your modifications make original images better, please, push those modifications back with some explanation why you made those changes.
