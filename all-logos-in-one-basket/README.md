
# :ferris_wheel: All logos in one basket

> a collection of Fediverse logos one might find useful

## WHY

1. These icons live in different corners of the web. Having them grouped in one place is pretty handy
2. All SVG icons here are minified, all have same size and zero-based viewbox;

## Contributing

Please, add logos of fully open source software only :) Suggestions welcome in issues or via merge requests.
