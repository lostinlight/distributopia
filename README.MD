
# Distributopia

`NOTE`: where possible, each subfolder has its own LICENSE file or README file where licenses are listed.

`NOTE`: This repository aggregates all the repos from old [Gitlab.com group](https://gitlab.com/distributopia). Updates will be merged only within this repo.

### [Table of Contents](#toc)

* [Fediverse paper toys](./papertoys)
* [Fediverse stickers and promo materials](./sticker-bay)
* [Fediverse SVG logos](./all-logos-in-one-basket)
* [Fediverse buttons](./share-connect-support)
* [Fediverse Pirate Party representatives](./caramba)
